﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace PolygonViewerApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private double dx;
        private double dy;

        private double defaultScale = 1;
        private double scale = 1;

        private System.Windows.Point lastMouseClick;

        private readonly Dictionary<string, Point> points = new Dictionary<string, Point>();
        private readonly Dictionary<string, PL> polylines = new Dictionary<string, PL>();

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = "XML files (*.xml)|*.xml",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            };

            if (openFileDialog.ShowDialog() == true)
            {
                ParseFile(openFileDialog.FileName);
            }
        }

        private void ParseFile(string filename)
        {
            XmlSerializer formatter;

            try
            {
                formatter = new XmlSerializer(typeof(UkrainianCadastralExchangeFile));
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid xml structure");
                return;
            }

            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                var data = (UkrainianCadastralExchangeFile)formatter.Deserialize(fs);

                KOATUU.Text = data.InfoPart.CadastralZoneInfo.KOATUU;
                CadastralZoneNumber.Text = data.InfoPart.CadastralZoneInfo.CadastralZoneNumber;
                CadastralQuarterNumber.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.CadastralQuarterNumber;

                Settlement.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelLocationInfo.Settlement;
                Region.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelLocationInfo.Region;
                StreetType.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelLocationInfo.ParcelAddress.StreetType;
                StreetName.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelLocationInfo.ParcelAddress.StreetName;
                Building.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelLocationInfo.ParcelAddress.Building;

                Size.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelMetricInfo.Area.Size;
                MeasurementUnit.Text = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.ParcelMetricInfo.Area.MeasurementUnit;

                var parcels = data.InfoPart.CadastralZoneInfo.CadastralQuarters.CadastralQuarterInfo.Parcels.ParcelInfo.LandsParcel.LandParcelInfo;

                points.Clear();

                var minX = Double.MaxValue;
                var maxX = Double.MinValue;

                var minY = Double.MaxValue;
                var maxY = Double.MinValue;

                var width = maxX - minX;
                var height = maxY - minY;

                foreach (var point in data.InfoPart.MetricInfo.PointInfo.Point)
                {
                    minX = Math.Min(minX, point.X);
                    maxX = Math.Max(maxX, point.X);

                    minY = Math.Min(minY, point.Y);
                    maxY = Math.Max(maxY, point.Y);

                    points[point.PN] = point;
                }

                polylines.Clear();
                Canvas.Children.Clear();

                foreach (var polyline in data.InfoPart.MetricInfo.Polyline.PL)
                {
                    var graphicsPolyline = new System.Windows.Shapes.Polyline
                    {
                        Stroke = Brushes.Pink,
                        StrokeThickness = 0.5,
                        Points = new PointCollection()
                    };

                    foreach (var pointId in polyline.Points.P)
                    {
                        var point = points[pointId];

                        var x = point.X - minX;
                        var y = point.Y - minY;

                        graphicsPolyline.Points.Add(new System.Windows.Point(x, -y));
                    }

                    graphicsPolyline.MouseLeftButtonDown += GraphicsPolyline_MouseLeftButtonDown;
                    graphicsPolyline.MouseLeftButtonUp += GraphicsPolyline_MouseLeftButtonUp;

                    polylines[polyline.ULID] = polyline;
                    Canvas.Children.Add(graphicsPolyline);
                }

                dx = 0;
                dy = 0;

                scale = defaultScale;

                CanvasScaleTransform.ScaleX = scale;
                CanvasScaleTransform.ScaleY = scale;
            }
        }

        private void GraphicsPolyline_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var s = sender as System.Windows.Shapes.Polyline;

            if (s != null)
            {
                s.Stroke = Brushes.Pink;
            }
        }

        private void GraphicsPolyline_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var s = sender as System.Windows.Shapes.Polyline;

            if (s != null)
            {
                s.Stroke = Brushes.Blue;
            }
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CanvasTranslateTransform.X = e.NewSize.Width / 2 + dx;
            CanvasTranslateTransform.Y = e.NewSize.Height / 2 + dy;

            CanvasScaleTransform.CenterX = CanvasTranslateTransform.X;
            CanvasScaleTransform.CenterY = CanvasTranslateTransform.Y;
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                lastMouseClick = e.GetPosition(null);
            }

            if (e.ClickCount == 2)
            {
                var ds = 1.2;

                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    scale *= ds;

                    CanvasScaleTransform.ScaleX = scale;
                    CanvasScaleTransform.ScaleY = scale;

                    e.Handled = true;
                }

                if (e.RightButton == MouseButtonState.Pressed)
                {
                    scale /= ds;

                    CanvasScaleTransform.ScaleX = scale;
                    CanvasScaleTransform.ScaleY = scale;

                    e.Handled = true;
                }
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var position = e.GetPosition(null);

                var md = 0.01;

                var mdx = (position.X - lastMouseClick.X) * md;
                var mdy = (position.Y - lastMouseClick.Y) * md;

                dx += mdx;
                dy += mdy;

                CanvasTranslateTransform.X = (Width / 2) + dx;
                CanvasTranslateTransform.Y = (Height / 2) + dy;

                CanvasScaleTransform.CenterX = CanvasTranslateTransform.X;
                CanvasScaleTransform.CenterY = CanvasTranslateTransform.Y;
            }
        }
    }
}
